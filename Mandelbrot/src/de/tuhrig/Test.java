package de.tuhrig;

import bb.util.Benchmark;

public class Test {

	public static void main(String[] agrs) {
		
		Runnable task = new Runnable() {

			public void run() {

				new Main().execute(1);
			}
		};
		
		Benchmark benchmark = new Benchmark(task);
		System.out.println(benchmark.toStringFull());
		
		task = new Runnable() {

			public void run() {

				new Main().execute(2);
			}
		};
		
		benchmark = new Benchmark(task);
		System.out.println(benchmark.toStringFull());
		
		task = new Runnable() {

			public void run() {

				new Main().execute(3);
			}
		};
		
		benchmark = new Benchmark(task);
		System.out.println(benchmark.toStringFull());
		
		task = new Runnable() {

			public void run() {

				new Main().execute(4);
			}
		};
		
		benchmark = new Benchmark(task);
		System.out.println(benchmark.toStringFull());
		
		task = new Runnable() {

			public void run() {

				new Main().execute(8);
			}
		};
		
		benchmark = new Benchmark(task);
		System.out.println(benchmark.toStringFull());
		
		task = new Runnable() {

			public void run() {

				new Main().execute(16);
			}
		};
		
		benchmark = new Benchmark(task);
		System.out.println(benchmark.toStringFull());
		
		task = new Runnable() {

			public void run() {

				new Main().execute(32);
			}
		};
		
		benchmark = new Benchmark(task);
		System.out.println(benchmark.toStringFull());
	}
}
