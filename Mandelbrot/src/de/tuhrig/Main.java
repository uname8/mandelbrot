package de.tuhrig;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

import javax.imageio.ImageIO;

public class Main {

	private static final double factor = 10500.0;
	
	private static final int x = (int) (2.6 * factor);
	
	private static final int y = (int) (2 * factor);
	
	private static final BufferedImage image = new BufferedImage(x, y, BufferedImage.TYPE_INT_RGB);
	
	private final ForkJoinPool fjPool = new ForkJoinPool();
	
	/**
	 * @param args
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws IOException, InterruptedException {

		double parts = 8;
	
		new Main().execute(parts);

		ImageIO.write(image, "png", new File(new Date().getTime() + ".png"));
	}

	public void execute(final double parts) {

		fjPool.invoke(new RecursiveAction() {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void compute() {

				List<Bound> map = new ArrayList<>();
				
				for(int i = 0; i < parts; i++) {
					
					Bound pair = new Bound();
					pair.b = 1 - ((2 / parts) * i);
					pair.a = 1 - ((2 / parts) + (2 / parts) * i);
					
					map.add(pair);
				}

				Mandelbrot task1 = new Mandelbrot(image.createGraphics(), factor);
				Mandelbrot task2 = new Mandelbrot(image.createGraphics(), factor);
				Mandelbrot task3 = new Mandelbrot(image.createGraphics(), factor);
				Mandelbrot task4 = new Mandelbrot(image.createGraphics(), factor);
				
				int count = 0;
				
				for(int i = 0; i < parts; i++) {
					
					if(count == 0)
						task1.add(map.get(i));
					if(count == 1)
						task2.add(map.get(i));
					if(count == 2)
						task3.add(map.get(i));
					if(count == 3)
						task4.add(map.get(i));
					
					count++;
					
					count = count%4;
				}
				
				invokeAll(task1, task2, task3, task4);
			}
		});
	}
}
